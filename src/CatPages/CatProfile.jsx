import React, { useState, useEffect } from "react";

const CatProfile = (props) => {
  let id = props.match.params.id;

  const [cats, setCats] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:3001/api/cats/${id}`);
      res
        .json()
        .then((res) => setCats(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [id]);

  return (
    <div>
      {cats.map((cat) => (
        <div key={cat.id}>
          <p>{cat.name}</p>
          <img src={cat.image} alt={cat.name} width="300" height="300" />
        </div>
      ))}
    </div>
  );
};

export default CatProfile;
