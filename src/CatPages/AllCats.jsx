import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

const AllCats = () => {
  const [cats, setCats] = useState([]);

  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:3001/api/cats");
      res
        .json()
        .then((res) => setCats(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);

  const catProfileRoute = (event, cat) => {
    event.preventDefault();
    let path = `/cats/${cat.id}`;
    history.push(path);
  };

  const addCatFormRoute = (event) => {
    event.preventDefault();
    let path = `/add-cat`;
    history.push(path);
  };

  return (
    <div>
      <button onClick={(e) => addCatFormRoute(e)}>Add a new Cat</button>
      <h1>Jen's Cat Adoption Cafe Website</h1>
      {cats.map((cat) => (
        <div key={cat.id}>
          <p>{cat.name}</p>
          <img src={cat.image} alt={cat.name} width="300" height="300" />
          <button onClick={(e) => catProfileRoute(e, cat)}>
            View Cat Profile
          </button>
        </div>
      ))}
    </div>
  );
};

export default AllCats;
