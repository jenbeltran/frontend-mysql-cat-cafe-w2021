import "./App.css";
import AllCats from "./CatPages/AllCats";
import CatProfile from "./CatPages/CatProfile";
import AddCatForm from "./CatPages/AddCatForm";
import { Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Route exact path="/" component={AllCats} />
      <Route exact path="/cats/:id" component={CatProfile} />
      <Route exact path="/add-cat" component={AddCatForm} />
    </div>
  );
}

export default App;
